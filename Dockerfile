FROM python:alpine

WORKDIR /code

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY code ./

CMD [ "python", "./test.py" ]